import React from 'react';
import './Grid.css';
import Space from '../Space/Space.js';
import Tile from '../Tile/Tile.js';

class Grid extends React.Component {

	/**
	 * Set the tile with a part of the full puzzle image.
	 *
	 * @param props
	 */
	constructor(props) {
		super(props);
		this.state = {
			tileImagePart: this.props.tileImagePart,
			tilePosition: this.props.tilePosition,
			isEmpty: this.props.isEmpty,
			spaces: spaces
		};
	}

	move(data) {
		return;
	}

	render () {
		let spaces = this.state.spaces.map((space, index) => {
			return (
				<Space key={space.spaceId}>
					{space.tileId ? <Tile key={space.tileId} onClick={this.move}></Tile> : null}
				</Space>
			);
		});
		return (
			<div className="grid">
				{spaces}
			</div>
		);
	}
}

export default Grid;
