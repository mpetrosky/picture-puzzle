import React from 'react';
import './Space.css';

class Space extends React.Component {
	/**
	 * Set the tile with a part of the full puzzle image.
	 *
	 * @param props
	 */
	constructor(props) {
		super(props);
		this.state = {
			tileImagePart: this.props.tileImagePart,
			tilePosition: this.props.tilePosition,
			isEmpty: this.props.isEmpty
		};
	}

	move(event) {
		this.setState({});
	}

	render() {
		return (
			<div className="space">{this.props.children}</div>
		);
	}
}

export default Space;
