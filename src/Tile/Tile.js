import React from 'react';
import './Tile.css';

class Tile extends React.Component {
	/**
	 * Set the tile with a part of the full puzzle image.
	 *
	 * @param props
	 */
	constructor(props) {
		super(props);
		this.state = {
			tileImagePart: this.props.tileImagePart,
			tilePosition: this.props.tilePosition
		};
	}

	componentDidUpdate() {
		if (this.props.onMove) {
			this.props.onMove(this.state);
		}
	}

	/**
	 * Move the tile to an empty space.
	 */
	move = (event): void => {
		this.setState(function(prevState, prevProps) {
			return {
				[event.target.name]: event.target.tilePosition
			};
		});
	}

	/**
	 * Render tile element.
	 *
	 * @return Element
	 */
	render() {
		return (
			<div className="tile" onClick={this.move}>
				<img src={this.state.tileImagePart} alt=""/>
			</div>
		);
	}
}

export default Tile;
